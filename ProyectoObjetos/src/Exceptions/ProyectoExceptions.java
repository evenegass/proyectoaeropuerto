/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import mensajes.Messages;

/**
 *
 * @author pc
 */
public class ProyectoExceptions extends Exception {
    
    private int numero;

    public ProyectoExceptions() {
        super();
    }

    public ProyectoExceptions(String message) {
        super(message);
    }

    public ProyectoExceptions(int numero) {
        this.numero = numero;
    }
public String numeroToString(){
  switch(numero){
      case 2627:
          return Messages.MNSJ_REG_REPETIDO;
      default:
          return Messages.MNSJ_EXITO;
  }  
}
}

