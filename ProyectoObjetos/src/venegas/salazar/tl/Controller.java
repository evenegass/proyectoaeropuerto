/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.tl;

import Exceptions.ProyectoExceptions;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import venegas.salazar.bl.dao.admin.Administrador;
import venegas.salazar.bl.dao.admin.IAdministrador;
import venegas.salazar.bl.dao.admin.SqlServerAdministradorDao;
import venegas.salazar.bl.dao.aeronave.Aeronave;
import venegas.salazar.bl.dao.aeronave.IAeronave;
import venegas.salazar.bl.dao.aeropuerto.Aeropuerto;
import venegas.salazar.bl.dao.aeropuerto.IAeropuerto;
import venegas.salazar.bl.dao.aeropuerto.SqlServerAeropuertoDao;
import venegas.salazar.bl.dao.factory.DaoFactory;
import venegas.salazar.bl.dao.factory.SqlServerDaoFactory;
import venegas.salazar.bl.dao.lineas.ILineaAerea;
import venegas.salazar.bl.dao.lineas.LineaAerea;
import venegas.salazar.bl.dao.lineas.SqlServerLineaAereaDao;
import venegas.salazar.bl.dao.miembro.IMiembro;
import venegas.salazar.bl.dao.miembro.Miembro;
import venegas.salazar.bl.dao.miembro.SqlServerMiembroDao;
import venegas.salazar.bl.dao.pais.IPais;
import venegas.salazar.bl.dao.pais.Pais;
import venegas.salazar.bl.dao.pais.SqlServerPaisDao;
import venegas.salazar.bl.dao.persona.IPersona;
import venegas.salazar.bl.dao.puertas.IPuertaDeAbordaje;
import venegas.salazar.bl.dao.puertas.PuertaDeAbordaje;
import venegas.salazar.bl.dao.puertas.SqlServerPuertaDeAbordajeDao;
import venegas.salazar.bl.dao.tiquete.ITiquete;
import venegas.salazar.bl.dao.tiquete.Tiquete;
import venegas.salazar.bl.dao.tripulacion.ITripulacion;
import venegas.salazar.bl.dao.tripulacion.Tripulacion;
import venegas.salazar.bl.dao.usuario.IUsuario;
import venegas.salazar.bl.dao.usuario.SqlServerUsuarioDao;
import venegas.salazar.bl.dao.usuario.Usuario;
import venegas.salazar.bl.dao.vuelo.IVuelo;
import venegas.salazar.bl.dao.vuelo.SqlServerVueloDao;
import venegas.salazar.bl.dao.vuelo.Vuelo;

/**
 *
 * @author pc
 */
public class Controller {

    DaoFactory factory;
    IAdministrador administrador;
    IAeronave aeronave;
    IAeropuerto aeropuerto;
    ILineaAerea linea;
    IMiembro miembro;
    IPais pais;
    IPersona persona;
    IPuertaDeAbordaje puerta;
    ITiquete tiquete;
    ITripulacion tripulacion;
    IUsuario usuario;
    IVuelo vuelo;

    SqlServerAdministradorDao logica = new SqlServerAdministradorDao();
    SqlServerUsuarioDao logica2 = new SqlServerUsuarioDao();
    SqlServerMiembroDao logica3 = new SqlServerMiembroDao();
    SqlServerPaisDao logica4 = new SqlServerPaisDao();
    SqlServerAeropuertoDao logica5 = new SqlServerAeropuertoDao();
    SqlServerPuertaDeAbordajeDao logica6 = new SqlServerPuertaDeAbordajeDao();
    SqlServerVueloDao logica7 = new SqlServerVueloDao();

    SqlServerLineaAereaDao logica0 = new SqlServerLineaAereaDao();

    public Controller() {
        // factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);//lo hace mas facil que estar digtandolo 
        //daoObject= factory.getClientes();
    }

    public void registrarAdministrador(Date fecNacimiento, int edad, String codigoAdmin,
            String nombre, String apellidos, String nacionalidad, String genero,
            String identificacion, String correo, String direccion) throws ProyectoExceptions, Exception {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAdministrador administrador = factory.getAdministrador();

        administrador.insertar(fecNacimiento, edad, codigoAdmin, nombre, apellidos, nacionalidad, genero, identificacion, correo, direccion);

    }

    public void registrarUsuario(String nombre, String apellidos, String nacionalidad, String genero, String identificacion, String correo, String direccion, String codigoUsuario) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUsuario usuario = factory.getUsuario();
        try {
            usuario.insertar(nombre, apellidos, nacionalidad, genero, identificacion, correo, direccion, codigoUsuario);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }
    }

    public void registrarMiembro(String nombre, String apellidos, String nacionalidad,
            String genero, String correo, String direccion, int anniosExp, Date fecGraduacion,
            String licencia, String puesto, String telefono, String codigoMiembro, String codigoTripulacion, String identificacion) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IMiembro miembro = factory.getMiembro();
        try {
            miembro.insertar(nombre, apellidos, nacionalidad, genero, correo, direccion, anniosExp, fecGraduacion, licencia, puesto, telefono, codigoMiembro, codigoTripulacion, identificacion);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }
    }

    public void registrarPais(String nombre, String abreviatura, String codigoPais, String codigoVuelo) throws ProyectoExceptions, Exception {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPais pais = factory.getPais();
        pais.insertar(nombre, abreviatura, codigoPais, codigoVuelo);
    }

    public void registrarAeropuertos(String nombre, String codigoAeropuerto, String pais, String codigoPais, String codigoAdministrador) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuerto aeropuerto = factory.getAeropuerto();
        try {
            aeropuerto.insertar(nombre, codigoAeropuerto, pais, codigoPais, codigoAdministrador);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }

    }

    public void registrarPuertas(String nombre, String codigoPuerta, String ubicacion) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPuertaDeAbordaje puerta = factory.getPuertasDeAbordaje();
        try {
            puerta.insertar(nombre, codigoPuerta, ubicacion);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }

    }

    public void registrarVuelos(String codigoAeropuerto, String paisOrigen,
            String precioTiquetes, String horaLlegada, String horaSalida,
            String estado, String aeropuertoPaisDestino, String codigoLinea, String codigoPuerta, String codigoVuelo, String codigoTiquete) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVuelo vuelo = factory.getVuelo();
        try {
            vuelo.insertar(codigoAeropuerto, paisOrigen, precioTiquetes, horaLlegada, horaSalida, estado, aeropuertoPaisDestino, codigoLinea, codigoPuerta, codigoVuelo, codigoTiquete);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }

    }

    public void registrarLineas(String nombreComercial, String cedulaJuridica,
            String nombreEmpresa, String codigoPais, String codigoLinea) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAerea linea = factory.getLineaAerea();
        try {
            linea.insertar(nombreComercial, cedulaJuridica, nombreEmpresa, codigoPais, codigoLinea);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }

    }

    public void registraAeronave(String placa, String marcaFab, String modelo,
            String capacidad, String codigoAeronave) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronave aeronave = factory.getAeronave();
        try {
            aeronave.insertar(placa, marcaFab, modelo, capacidad, codigoAeronave);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }

    }

    public void registrarTiquete(Date fecha, String lugarDeSalida, String lugarDondeVa,
            int numAsieto, String precio, int numVuelo, String tipoAsiento, int impuesto, String estado, String codigoTiquete, String codigoPais, String codigoAeropuerto, String codigoVuelo) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITiquete tiquete = factory.getTiquete();
        try {
            tiquete.insertar(fecha, lugarDeSalida, lugarDondeVa,
                    numAsieto, precio, numVuelo, tipoAsiento, impuesto, estado, codigoTiquete, codigoPais, codigoAeropuerto, codigoVuelo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }

    }

    public void registrarTripulacion(String codigoTripulacion, String nombreClave) throws ProyectoExceptions {

        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacion tripulacion = factory.getTripulacion();
        try {
            tripulacion.insertar(codigoTripulacion, nombreClave);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            ProyectoExceptions exc = new ProyectoExceptions(error);
            throw exc;
            //return exc.numeroToString(); no compila por ser string se cmbia
        } catch (Exception e) {
            ProyectoExceptions exc = new ProyectoExceptions(e.getMessage());
            throw exc;
        }
    }

    public ArrayList<Administrador> getAdministrador() throws Exception {
        ArrayList<Administrador> getAdministrador = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IAdministrador daoObject = factory.getAdministrador();
            getAdministrador = daoObject.getAdministrador();
            return getAdministrador;
        } catch (Exception e) {
            e.getMessage();
        }
        return getAdministrador;
    }

    public ArrayList<Usuario> getUsuario() throws Exception {
        ArrayList<Usuario> getUsuario = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IUsuario daoObject = factory.getUsuario();
            getUsuario = daoObject.getUsuario();
            return getUsuario;
        } catch (Exception e) {
            e.getMessage();
        }
        return getUsuario;
    }

    public ArrayList<Aeronave> getAeronave() throws Exception {
        ArrayList<Aeronave> getAeronave = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IAeronave daoObject = factory.getAeronave();
            getAeronave = daoObject.getAeronave();
            return getAeronave;
        } catch (Exception e) {
            e.getMessage();
        }
        return getAeronave;
    }

    public ArrayList<Aeropuerto> getAeropuerto() throws Exception {
        ArrayList<Aeropuerto> getAeropuerto = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IAeropuerto daoObject = factory.getAeropuerto();
            getAeropuerto = daoObject.getAeropuerto();
            return getAeropuerto;
        } catch (Exception e) {
            e.getMessage();
        }
        return getAeropuerto;
    }
    
      public ArrayList<LineaAerea> getLineaAerea() throws Exception {
        ArrayList<LineaAerea> getLineaAerea = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            ILineaAerea daoObject = factory.getLineaAerea();
            getLineaAerea = daoObject.getLineaAerea();
            return getLineaAerea;
        } catch (Exception e) {
            e.getMessage();
        }
        return getLineaAerea;
    }
          public ArrayList<Miembro> getMiembro() throws Exception {
        ArrayList<Miembro> getMiembro = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IMiembro daoObject = factory.getMiembro();
            getMiembro = daoObject.getMiembro();
            return getMiembro;
        } catch (Exception e) {
            e.getMessage();
        }
        return getMiembro;
    }
          
             public ArrayList<Pais> getPais() throws Exception {
        ArrayList<Pais> getPais = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IPais daoObject = factory.getPais();
            getPais = daoObject.getPais();
            return getPais;
        } catch (Exception e) {
            e.getMessage();
        }
        return getPais;
    }
         public ArrayList<PuertaDeAbordaje> getPuertasDeAbordaje() throws Exception {
        ArrayList<PuertaDeAbordaje> getPuertasDeAbordaje = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IPuertaDeAbordaje daoObject = factory.getPuertasDeAbordaje();
            getPuertasDeAbordaje = daoObject.getPuertasDeAbordaje();
            return getPuertasDeAbordaje;
        } catch (Exception e) {
            e.getMessage();
        }
        return getPuertasDeAbordaje;
    }   
         
            public ArrayList<Tiquete> getTiquete() throws Exception {
        ArrayList<Tiquete> getTiquete = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            ITiquete daoObject = factory.getTiquete();
            getTiquete = daoObject.getTiquete();
            return getTiquete;
        } catch (Exception e) {
            e.getMessage();
        }
        return getTiquete;
    } 
         public ArrayList<Tripulacion> getTripulacion() throws Exception {
        ArrayList<Tripulacion> getTripulacion = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            ITripulacion daoObject = factory.getTripulacion();
            getTripulacion = daoObject.getTripulacion();
            return getTripulacion;
        } catch (Exception e) {
            e.getMessage();
        }
        return getTripulacion;
    }
          public ArrayList<Vuelo> getVuelo() throws Exception {
        ArrayList<Vuelo> getVuelo = new ArrayList<>();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IVuelo daoObject = factory.getVuelo();
            getVuelo = daoObject.getVuelo();
            return getVuelo;
        } catch (Exception e) {
            e.getMessage();
        }
        return getVuelo;
    }
      
      
      
}
