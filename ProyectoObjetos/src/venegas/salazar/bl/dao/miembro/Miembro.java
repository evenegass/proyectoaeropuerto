package venegas.salazar.bl.dao.miembro;

import java.sql.Date;
import java.time.LocalDate;
import venegas.salazar.bl.dao.persona.Persona;

public class Miembro extends Persona{

    private int anniosExp;
    private Date fecGraduacion;
    private String licencia;
    private String puesto;
    private String telefono;
    private String codigoMiembro;
    private String codigoTripulacion;
    public Miembro() {
    }

    public Miembro(String nombre, String apellidos, String nacionalidad,
            String genero, String correo, String direccion,int anniosExp, Date fecGraduacion,
            String licencia, String puesto, String telefono, String codigoMiembro, String codigoTripulacion,String identificacion) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nacionalidad = nacionalidad;
        this.genero = genero;
       
        this.correo = correo;
        this.direccion = direccion;
        this.anniosExp = anniosExp;
        this.fecGraduacion = fecGraduacion;
        this.licencia = licencia;
        this.puesto = puesto;
        this.telefono = telefono;
        this.codigoMiembro= codigoMiembro;
        this.codigoTripulacion= codigoTripulacion;
         this.identificacion = identificacion;
        
    }

    public int getAnniosExp() {
        return anniosExp;
    }

    public void setAnniosExp(int anniosExp) {
        this.anniosExp = anniosExp;
    }

    public Date getFecGraduacion() {
        return fecGraduacion;
    }

    public void setFecGraduacion(Date fecGraduacion) {
        this.fecGraduacion = fecGraduacion;
    }

    public String getLicencia() {
        return licencia;
    }

    public void setLicencia(String licencia) {
        this.licencia = licencia;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCodigoMiembro() {
        return codigoMiembro;
    }

    public void setCodigoMiembro(String codigoMiembro) {
        this.codigoMiembro = codigoMiembro;
    }

    public String getCodigoTripulacion() {
        return codigoTripulacion;
    }

    public void setCodigoTripulacion(String codigoTripulacion) {
        this.codigoTripulacion = codigoTripulacion;
    }

    @Override
    public String toString() {
        return "Miembro{" + "anniosExp=" + anniosExp + ", fecGraduacion=" + fecGraduacion + ", licencia=" + licencia + ", puesto=" + puesto + ", telefono=" + telefono + ", codigoMiembro=" + codigoMiembro + ", codigoTripulacion=" + codigoTripulacion + '}';
    }
    

}
