
package venegas.salazar.bl.dao.miembro;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;


public interface IMiembro {
     void insertar(String nombre, String apellidos, String nacionalidad,
            String genero, String correo, String direccion,int anniosExp, Date fecGraduacion,
            String licencia, String puesto, String telefono, String codigoMiembro, String codigoTripulacion, String identificacion)throws SQLException,Exception;
      ArrayList<Miembro> getMiembro() throws SQLException, Exception;
}
