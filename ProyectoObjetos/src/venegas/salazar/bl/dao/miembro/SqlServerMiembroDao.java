/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.miembro;

import accesodatos.Conector;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerMiembroDao implements IMiembro {

    @Override
    public void insertar(String nombre, String apellidos, String nacionalidad,
            String genero, String correo, String direccion, int anniosExp, Date fecGraduacion,
            String licencia, String puesto, String telefono, String codigoMiembro, String codigoTripulacion, String identificacion)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO MIEMBRO (NOMBRE,APELLIDOS,NACIONALIDAD,GENERO,CORREO,DIRECCION,ANNIOS_EXP,FEC_GRADUACION,LICENCIA,PUESTO,TELEFONO, CODIGO_MIEMBRO, CODIGO_TRIPULACION,IDENTIFICACION"
                + "VALUES ('" + nombre + ",'" + apellidos + "','" + nacionalidad
                + "','" + genero + "','" + "','" + correo + "','" + direccion + "','" + anniosExp + "','"
                + fecGraduacion + "','" + licencia + "','" + puesto + "','" + telefono + "','" + codigoMiembro + "','" + codigoTripulacion + "','" + identificacion + "')";

        Conector.getConector().ejectuarSql(queryText);
    }

    @Override
    public ArrayList<Miembro> getMiembro() throws Exception {
        ArrayList<Miembro> miembro = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT NOMBRE,APELLIDOS,NACIONALIDAD,GENERO,CORREO,DIRECCION,ANNIOS_EXP,FEC_GRADUACION,LICENCIA,PUESTO,TELEFONO, CODIGO_MIEMBRO, CODIGO_TRIPULACION,IDENTIFICACION FROM MIEMBRO ";
        //CONECTARBD
        rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Miembro tmpMiembro = new Miembro(
                    rs.getString("NOMBRE"),
                    rs.getString("APELLIDOS"),
                    rs.getString("NACIONALIDAD"),
                    rs.getString("GENERO"),
                    rs.getString("CORREO"),
                    rs.getString("DIRECCION"),
                    rs.getInt("ANNIOS_EXP"),
                    rs.getDate("FEC_GRADUACION"),
                    rs.getString("LICENCIA"),
                    rs.getString("PUESTO"),
                    rs.getString("TELEFONO"),
                    rs.getString("CODIGO_MIEMBRO"),
                    rs.getString("CODIGO_TRIPULACION"),
                    rs.getString("IDENTIFICACION"));
            miembro.add(tmpMiembro);

        }

        return miembro;

    }

}
