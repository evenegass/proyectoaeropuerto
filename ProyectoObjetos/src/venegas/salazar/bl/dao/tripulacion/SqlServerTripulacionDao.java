/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.tripulacion;

import accesodatos.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerTripulacionDao implements ITripulacion{
    
  @Override
  public void insertar(String codigoTripulacion, String nombreClave)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO TRIPULACION (CODIGO_TRIPULACION,NOMBRE_CLAVE"
                + "VALUES ('" + codigoTripulacion + ",'" + nombreClave + "')";

        Conector.getConector().ejectuarSql(queryText);
    }

  @Override
    public ArrayList<Tripulacion> getTripulacion() throws Exception {
        ArrayList<Tripulacion> tripulacion = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT CODIGO_TRIPULACION,NOMBRE_CLAVE FROM TRIPULACION ";
        //CONECTARBD
        rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Tripulacion tmpTripulacion = new Tripulacion(
                  
                    rs.getString("CODIGO_TRIPULACION"),
                    rs.getString("NOMBRE_CLAVE"));
            tripulacion.add(tmpTripulacion);

        }

        return tripulacion;

    }
}

