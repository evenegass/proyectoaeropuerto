
package venegas.salazar.bl.dao.tripulacion;

public class Tripulacion {
    private String codigoTripulacion;
    private String nombreClave;

    public Tripulacion() {
    }

    public Tripulacion(String codigoTripulacion, String nombreClave) {
        this.codigoTripulacion = codigoTripulacion;
        this.nombreClave = nombreClave;
    }

    public String getCodigoTripulacion() {
        return codigoTripulacion;
    }

    public void setCodigo(String codigoTripulacion) {
        this.codigoTripulacion = codigoTripulacion;
    }

    public String getNombreClave() {
        return nombreClave;
    }

    public void setNombreClave(String nombreClave) {
        this.nombreClave = nombreClave;
    }

    @Override
    public String toString() {
        return "Tripulcion{" + "codigo Tripulacion=" + codigoTripulacion 
                + ", nombreClave=" + nombreClave + '}';
    }
    
    
}
