/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.tripulacion;

import java.sql.SQLException;
import java.util.ArrayList;



/**
 *
 * @author pc
 */
public interface ITripulacion {
     void insertar(String codigoTripulacion, String nombreClave)throws SQLException,Exception;
      ArrayList<Tripulacion> getTripulacion() throws SQLException, Exception;
}
