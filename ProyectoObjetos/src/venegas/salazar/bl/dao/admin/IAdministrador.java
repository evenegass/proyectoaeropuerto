/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.admin;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IAdministrador {
      public void insertar(Date fecNacimiento, int edad,String codigoAdmin,
            String nombre, String apellidos, String nacionalidad,String genero,
            String identificacion,  String correo, String direccion)throws SQLException,Exception;
      ArrayList<Administrador> getAdministrador() throws SQLException, Exception;

}
