package venegas.salazar.bl.dao.admin;

import java.sql.Date;

import venegas.salazar.bl.dao.persona.Persona;

public class Administrador extends Persona {

    private Date fecNacimiento;
    private int edad;
    private String codigoAdmin;

    public Administrador() {
        super();
    }

    public Administrador(Date fecNacimiento, int edad,String codigoAdmin,
            String nombre, String apellidos, String nacionalidad,String genero,String identificacion,  String correo, String direccion) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nacionalidad = nacionalidad;
        this.genero = genero;
        this.identificacion = identificacion;
        this.correo = correo;
        this.direccion = direccion;
        this.fecNacimiento = fecNacimiento;
        this.edad = edad;
        this.codigoAdmin=codigoAdmin;

    }

    

   

    public Date getFecNacimiento() {
        return fecNacimiento;
    }

    public void setFecNacimiento(Date fecNacimiento) {
        this.fecNacimiento = fecNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCodigoAdmin() {
        return codigoAdmin;
    }

    public void setCodigoAdmin(String codigoAdmin) {
        this.codigoAdmin = codigoAdmin;
    }

    @Override
    public String toString() {
        return "Administrador{" + "fecNacimiento=" + fecNacimiento + ", edad=" + edad + ", codigoAdmin=" + codigoAdmin + '}';
    }
    

    

}
