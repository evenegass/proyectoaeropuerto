/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.admin;

import accesodatos.Conector;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerAdministradorDao implements IAdministrador {

    @Override
    public void insertar(Date fecNacimiento, int edad,String codigoAdmin,
            String nombre, String apellidos, String nacionalidad,String genero,
            String identificacion,  String correo, String direccion)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO ADMINISTRADOR (FECNACIMIENTO,EDAD,CODIGO_ADMIN"
                + ",NOMBRE, APELLIDOS, NACIONALIDAD, GENERO, IDENTIFICACION, CORREO, DIRECCION)"
                + "VALUES ('" + fecNacimiento + "','" + edad + "','" + codigoAdmin
                + "','" + nombre + "','" + apellidos + "','" + nacionalidad + "','" + genero + "','" + identificacion + "','" + correo +"','"+direccion+ "')";

        Conector.getConector().ejectuarSql(queryText);
    }

    @Override
    public ArrayList<Administrador> getAdministrador() throws Exception {
        ArrayList<Administrador> administrador = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT FECNACIMIENTO,EDAD,CODIGO_ADMIN,NOMBRE, APELLIDOS, NACIONALIDAD, GENERO, IDENTIFICACION, CORREO, DIRECCION FROM ADMINISTRADOR ";
      //CONECTARBD
      rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Administrador tmpAdmin = new Administrador(
                    rs.getDate("FECNACIMIENTO"),
                    rs.getInt("EDAD"),
                    rs.getString("CODIGO_ADMIN"),
                    rs.getString("NOMBRE"),
                    rs.getString("APELLIDOS"),
                    rs.getString("NACIONALIDAD"),
                    rs.getString("GENERO"),
                    rs.getString("IDENTIFICACION"),
                     rs.getString("CORREO"),
                    rs.getString("DIRECCION"));
            administrador.add(tmpAdmin);

        }

        return administrador;

    }

}
