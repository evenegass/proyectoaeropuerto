package venegas.salazar.bl.dao.tiquete;

import java.sql.Date;
import java.time.LocalDate;

public class Tiquete {

    private Date fecha;
    private String lugarDeSalida;
    private String lugarDondeVa;
    private int numAsieto;
    private String precio;
    private int numVuelo;
    private String tipoAsiento;
    private int impuesto;
    private String estado;
    private String codigoTiquete;
    private String codigoVuelo;
    private String codigoPais;
    private String codigoAeropuerto;

    public Tiquete() {
    }

    public Tiquete(Date fecha, String lugarDeSalida, String lugarDondeVa,
            int numAsieto, String precio, int numVuelo, String tipoAsiento, int impuesto, String estado, String codigoTiquete, String codigoPais, String codigoAeropuerto, String codigoVuelo) {
        this.fecha = fecha;
        this.lugarDeSalida = lugarDeSalida;
        this.lugarDondeVa = lugarDondeVa;
        this.numAsieto = numAsieto;
        this.precio = precio;
        this.numVuelo = numVuelo;
        this.tipoAsiento = tipoAsiento;
        this.impuesto = impuesto;
        this.estado = estado;
        this.codigoTiquete = codigoTiquete;
        this.codigoPais = codigoPais;
        this.codigoAeropuerto = codigoAeropuerto;
        this.codigoVuelo = codigoVuelo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLugarDeSalida() {
        return lugarDeSalida;
    }

    public void setLugarDeSalida(String lugarDeSalida) {
        this.lugarDeSalida = lugarDeSalida;
    }

    public String getLugarDondeVa() {
        return lugarDondeVa;
    }

    public void setLugarDondeVa(String lugarDondeVa) {
        this.lugarDondeVa = lugarDondeVa;
    }

    public int getNumAsieto() {
        return numAsieto;
    }

    public void setNumAsieto(int numAsieto) {
        this.numAsieto = numAsieto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getNumVuelo() {
        return numVuelo;
    }

    public void setNumVuelo(int numVuelo) {
        this.numVuelo = numVuelo;
    }

    public String getTipoAsiento() {
        return tipoAsiento;
    }

    public void setTipoAsiento(String tipoAsiento) {
        this.tipoAsiento = tipoAsiento;
    }

    public int getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(int impuesto) {
        this.impuesto = impuesto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoTiquete() {
        return codigoTiquete;
    }

    public void setCodigoTiquete(String codigoTiquete) {
        this.codigoTiquete = codigoTiquete;
    }

    public String getCodigoVuelo() {
        return codigoVuelo;
    }

    public void setCodigoVuelo(String codigoVuelo) {
        this.codigoVuelo = codigoVuelo;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getCodigoAeropuerto() {
        return codigoAeropuerto;
    }

    public void setCodigoAeropuerto(String codigoAeropuerto) {
        this.codigoAeropuerto = codigoAeropuerto;
    }

    @Override
    public String toString() {
        return "Tiquete{" + "fecha=" + fecha + ", lugarDeSalida=" + lugarDeSalida + ", lugarDondeVa=" + lugarDondeVa + ", numAsieto=" + numAsieto + ", precio=" + precio + ", numVuelo=" + numVuelo + ", tipoAsiento=" + tipoAsiento + ", impuesto=" + impuesto + ", estado=" + estado + ", codigoTiquete=" + codigoTiquete + ", codigoVuelo=" + codigoVuelo + ", codigoPais=" + codigoPais + ", codigoAeropuerto=" + codigoAeropuerto + '}';
    }
    

    
}
