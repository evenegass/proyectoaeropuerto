/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.tiquete;

import accesodatos.Conector;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerTiqueteDao implements ITiquete {

    @Override
    public void insertar(Date fecha, String lugarDeSalida, String lugarDondeVa,
            int numAsieto, String precio, int numVuelo, String tipoAsiento, int impuesto, String estado, String codigoTiquete,String codigoPais, String codigoAeropuerto, String codigoVuelo)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO TIQUETE (FECHA,LUGAR_SALIDA, LUGAR_DONDE_VA, NUM_ASIENTO, PRECIO, NUM_VUELO,TIPO_ASIENTO, IMPUESTO, ESTADO, CODIGO_TIQUETE,CODIGO_PAIS,CODIGO_AEROPUERTO, CODIGO_VUELO"
                + "VALUES ('" + fecha + "','" + lugarDeSalida + "','" + lugarDondeVa + "','" + numAsieto + "','" + precio + "','" + numVuelo + "','" + tipoAsiento + "','" + impuesto + "','"+estado + "','"+codigoTiquete + "','"+codigoPais+"','"+codigoAeropuerto+"','" + codigoVuelo + "')";

        Conector.getConector().ejectuarSql(queryText);
    }

    @Override
    public ArrayList<Tiquete> getTiquete() throws Exception {
        ArrayList<Tiquete> tiquete = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT FECHA,LUGAR_SALIDA, LUGAR_DONDE_VA, NUM_ASIENTO, PRECIO, NUM_VUELO,TIPO_ASIENTO, IMPUESTO, ESTADO, CODIGO_TIQUETE,CODIGO_PAIS,CODIGO_AEROPUERTO, CODIGO_VUELO FROM TIQUETE ";
        //CONECTARBD
        rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Tiquete tmpTiquete = new Tiquete(
                    rs.getDate("FECHA"),
                    rs.getString("LUGAR_SALIDA"),
                    rs.getString("LUGAR_DONDE_VA"),
                    rs.getInt(" NUM_ASIENTO"),
                    rs.getString(" PRECIO"),
                    rs.getInt(" NUM_VUELO"),
                    rs.getString("TIPO_ASIENTO"),
                    rs.getInt("IMPUESTO"),
                     rs.getString(" ESTADO"),
                    rs.getString("CODIGO_TIQUETE"),
                     rs.getString("CODIGO_PAIS"),
                     rs.getString("CODIGO_AEROPUERTO"),
                    rs.getString("CODIGO_VUELO"));
            tiquete.add(tmpTiquete);

        }

        return tiquete;

    }
}
