/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.tiquete;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface ITiquete {
    void insertar(Date fecha, String lugarDeSalida, String lugarDondeVa,
            int numAsieto, String precio, int numVuelo, String tipoAsiento, int impuesto, String estado, String codigoTiquete,String codigoPais, String codigoAeropuerto, String codigoVuelo)throws SQLException,Exception;
      ArrayList<Tiquete> getTiquete() throws SQLException, Exception;
}
