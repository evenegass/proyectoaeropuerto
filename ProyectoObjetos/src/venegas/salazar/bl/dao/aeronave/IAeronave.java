/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.aeronave;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IAeronave {
    public void insertar(String placa, String marcaFab, String modelo, String capacidad,String codigoAeronave)throws SQLException,Exception;
      ArrayList<Aeronave> getAeronave() throws SQLException, Exception;
}
