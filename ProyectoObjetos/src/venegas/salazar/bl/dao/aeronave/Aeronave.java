
package venegas.salazar.bl.dao.aeronave;

public class Aeronave {
   private String placa;
   private String marcaFab;
   private String modelo;
   private String capacidad;
   private String codigoAeronave;

    public Aeronave() {
    }

    public Aeronave(String placa, String marcaFab, String modelo, String capacidad, String codigoAeronave) {
        this.placa = placa;
        this.marcaFab = marcaFab;
        this.modelo = modelo;
        this.capacidad = capacidad;
        this.codigoAeronave= codigoAeronave;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarcaFab() {
        return marcaFab;
    }

    public void setMarcaFab(String marcaFab) {
        this.marcaFab = marcaFab;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad = capacidad;
    }

    public String getCodigoAeronave() {
        return codigoAeronave;
    }

    public void setCodigoAeronave(String codigoAeronave) {
        this.codigoAeronave = codigoAeronave;
    }

    @Override
    public String toString() {
        return "Aeronave{" + "placa=" + placa + ", marcaFab=" + marcaFab + ", modelo=" + modelo + ", capacidad=" + capacidad + ", codigoAeronave=" + codigoAeronave + '}';
    }
    

  
   
   
}
