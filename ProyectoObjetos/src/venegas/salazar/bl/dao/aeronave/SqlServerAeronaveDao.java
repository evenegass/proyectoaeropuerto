/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.aeronave;

import accesodatos.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerAeronaveDao implements IAeronave {
    
    @Override
    public void insertar(String placa, String marcaFab, String modelo, String capacidad, String codigoAeronave)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO AERONAVE (PLACA,MARCA_FAB,MODELO"
                + ",CAPACIDAD, CODIGO_AERONAVE"
                + "VALUES ('" + placa + ",'" + marcaFab + "','" + modelo
                + "','" + capacidad+"','"+codigoAeronave+ "')";

        Conector.getConector().ejectuarSql(queryText);
    }
    @Override
     public ArrayList<Aeronave> getAeronave() throws Exception {
        ArrayList<Aeronave> aeronaves = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT PLACA, MARCA_FAb, MODELO, CAPACIDAD, CODIGO_AERONAVE FROM AERONAVE ";
      //CONECTARBD
      rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Aeronave tmpAero = new Aeronave(
                    rs.getString("PLACA"),
                    rs.getString("MARCA_FAb"),
                    rs.getString("MODELO"),
                     rs.getString("CAPACIDAD"),
                    rs.getString("CODIGO_AERONAVE"));
            aeronaves.add(tmpAero);

        }

        return aeronaves;

    }

}


    

