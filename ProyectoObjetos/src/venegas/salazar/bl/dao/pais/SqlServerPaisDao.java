/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.pais;

import accesodatos.Conector;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerPaisDao implements IPais {

    @Override
    public void insertar(String nombre, String abreviatura, String codigoPais, String codigoVuelo)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO PAIS (NOMBRE,ABREVIATURA, CODIGO_PAIS, CODIGO_VUELO)"
                + "VALUES ('" + nombre + "','" + abreviatura + "','" + codigoPais + "','" + codigoVuelo + "')";

        Conector.getConector().ejectuarSql(queryText);
    }

    @Override
    public ArrayList<Pais> getPais() throws Exception {
        ArrayList<Pais> pais = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT NOMBRE, ABREVIATURA , CODIGO_PAIS, CODIGO_VUELO FROM PAIS ";
        //CONECTARBD
        rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Pais tmpPais = new Pais(
                    rs.getString("NOMBRE"),
                    rs.getString("ABREVIATURA"),
                    rs.getString("CODIGO_PAIS"),
                    rs.getString("CODIGO_VUELO"));
            pais.add(tmpPais);

        }

        return pais;

    }

}
