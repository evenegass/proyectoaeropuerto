
package venegas.salazar.bl.dao.pais;


public class Pais {
   private String nombre;
   private String abreviatura;
   private String codigoPais;
   private String codigoVuelo;

    public Pais() {
    }

    public Pais(String nombre, String abreviatura, String codigoPais, String codigoVuelo) {
        this.nombre = nombre;
        this.abreviatura = abreviatura;
        this.codigoPais= codigoPais;
        this.codigoVuelo= codigoVuelo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getCodigoVuelo() {
        return codigoVuelo;
    }

    public void setCodigoVuelo(String codigoVuelo) {
        this.codigoVuelo = codigoVuelo;
    }

    @Override
    public String toString() {
        return "Pais{" + "nombre=" + nombre + ", abreviatura=" + abreviatura + ", codigoPais=" + codigoPais + ", codigoVuelo=" + codigoVuelo + '}';
    }
    


   
}
