/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.pais;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IPais {
     void insertar(String nombre, String abreviatura, String codigoPais, String codigoVuelo)throws SQLException,Exception;
      ArrayList<Pais> getPais() throws SQLException, Exception;
}
