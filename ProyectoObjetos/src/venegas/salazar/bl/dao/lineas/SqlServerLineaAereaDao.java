/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.lineas;

import accesodatos.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerLineaAereaDao implements ILineaAerea {
     @Override
     public void insertar(String nombreComercial, String cedulaJuridica, String nombreEmpresa, String codigoPais, String codigoLinea)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO LINEAS_AEREAS (NOMBRE_COMERCIAL,CEDULA_JURIDICA,NOMBRE_EMPRESA"
                + ",CODIGO_PAIS, CODIGO_LINEA"
                + "VALUES ('" + nombreComercial + ",'" + cedulaJuridica + "','" + nombreEmpresa
                + "','" + codigoPais+"','"+codigoLinea+ "')";

        Conector.getConector().ejectuarSql(queryText);
    }
     @Override
     public ArrayList<LineaAerea> getLineaAerea() throws Exception {
        ArrayList<LineaAerea> linea = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT NOMBRE_COMERCIAL, CEDULA_JURIDICA, NOMBRE_EMPRESA, CODIGO_PAIS, CODIGO_LINEA FROM LINEAS_AEREAS  ";
      //CONECTARBD
      rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            LineaAerea tmpLinea = new LineaAerea(
                    rs.getString("NOMBRE_COMERCIAL"),
                    rs.getString("CEDULA_JURIDICA"),
                    rs.getString("NOMBRE_EMPRESA"),
                     rs.getString("CODIGO_PAIS"),
                    rs.getString("CODIGO_LINEA"));
            linea.add(tmpLinea);

        }

        return linea;

    }

}
