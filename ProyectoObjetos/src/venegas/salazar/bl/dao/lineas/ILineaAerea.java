/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.lineas;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface ILineaAerea {
    void insertar(String nombreComercial, String cedulaJuridica, String nombreEmpresa, String codigoPais, String codigoLinea)throws SQLException,Exception;
      ArrayList<LineaAerea> getLineaAerea() throws SQLException, Exception;
}
