package venegas.salazar.bl.dao.lineas;

public class LineaAerea {

    private String nombreComercial;
    private String cedulaJuridica;
    private String nombreEmpresa;
    private String codigoPais;
    private String codigoLinea;
    //private String logo;

    public LineaAerea() {
    }

    public LineaAerea(String nombreComercial, String cedulaJuridica, String nombreEmpresa, String codigoPais,String codigoLinea/*, String logo*/) {
        this.nombreComercial = nombreComercial;
        this.cedulaJuridica = cedulaJuridica;
        this.nombreEmpresa = nombreEmpresa;
        this.codigoPais = codigoPais;
        this.codigoLinea= codigoLinea;
       // this.logo = logo;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getCedulaJuridica() {
        return cedulaJuridica;
    }

    public void setCedulaJuridica(String cedulaJuridica) {
        this.cedulaJuridica = cedulaJuridica;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getCodigoLinea() {
        return codigoLinea;
    }

    public void setCodigoLinea(String codigoLinea) {
        this.codigoLinea = codigoLinea;
    }

    /*public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }*/

    @Override
    public String toString() {
        return "LineasAereas{" + "nombreComercial=" + nombreComercial + ", cedulaJuridica=" + cedulaJuridica + ", nombreEmpresa=" + nombreEmpresa + ", pais=" + codigoPais +/* ", logo=" + logo +*/ '}';
    }
    
    

}
