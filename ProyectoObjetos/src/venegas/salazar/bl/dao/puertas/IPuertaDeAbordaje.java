/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.puertas;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IPuertaDeAbordaje {
     void insertar(String nombre, String codigo, String ubicacion)throws SQLException,Exception;
  ArrayList<PuertaDeAbordaje> getPuertasDeAbordaje() throws SQLException, Exception;
}
