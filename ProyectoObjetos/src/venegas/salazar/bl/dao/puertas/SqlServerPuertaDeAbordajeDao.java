/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.puertas;

import accesodatos.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerPuertaDeAbordajeDao implements IPuertaDeAbordaje{
     @Override
     public void insertar(String nombre, String codigo, String ubicacion)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO PUERTAS_DE_ABORDAJE (NOMBRE,CODIGO, UBICACION"
                + "VALUES ('" + nombre + ",'" + codigo+",'"+ubicacion+"')";

        Conector.getConector().ejectuarSql(queryText);
    }

     @Override
    public ArrayList<PuertaDeAbordaje> getPuertasDeAbordaje() throws Exception {
        ArrayList<PuertaDeAbordaje> puerta = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT NOMBRE,CODIGO, UBICACION FROM PUERTAS_DE_ABORDAJE ";
        //CONECTARBD
        rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            PuertaDeAbordaje tmpPuerta= new PuertaDeAbordaje(
                    rs.getString("NOMBRE"),
                       rs.getString("CODIGO"),
                       rs.getString("UBICACION"));
            puerta.add(tmpPuerta);

        }

        return puerta;

    }
}
