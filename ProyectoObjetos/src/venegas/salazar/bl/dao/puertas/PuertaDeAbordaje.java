package venegas.salazar.bl.dao.puertas;

public class PuertaDeAbordaje {

    protected String nombre;
    protected String codigoPuerta;
    protected String ubicacion;

    public PuertaDeAbordaje() {
    }

    public PuertaDeAbordaje(String nombre, String codigoPuerta, String ubicacion) {
        this.nombre = nombre;
        this.codigoPuerta = codigoPuerta;
        this.ubicacion = ubicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoPuerta() {
        return codigoPuerta;
    }

    public void setCodigoPuerta(String codigoPuerta) {
        this.codigoPuerta = codigoPuerta;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public String toString() {
        return "PuertasDeAbordaje{" + "nombre=" + nombre + ", codigo=" + codigoPuerta + ", ubicacion=" + ubicacion + '}';
    }
    

}
