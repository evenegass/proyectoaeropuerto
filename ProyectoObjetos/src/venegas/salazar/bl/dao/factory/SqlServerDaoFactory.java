/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.factory;

import venegas.salazar.bl.dao.admin.IAdministrador;
import venegas.salazar.bl.dao.admin.SqlServerAdministradorDao;
import venegas.salazar.bl.dao.aeronave.IAeronave;
import venegas.salazar.bl.dao.aeronave.SqlServerAeronaveDao;
import venegas.salazar.bl.dao.aeropuerto.IAeropuerto;
import venegas.salazar.bl.dao.aeropuerto.SqlServerAeropuertoDao;
import venegas.salazar.bl.dao.lineas.ILineaAerea;
import venegas.salazar.bl.dao.lineas.SqlServerLineaAereaDao;
import venegas.salazar.bl.dao.miembro.IMiembro;
import venegas.salazar.bl.dao.miembro.SqlServerMiembroDao;
import venegas.salazar.bl.dao.pais.IPais;
import venegas.salazar.bl.dao.pais.SqlServerPaisDao;
import venegas.salazar.bl.dao.persona.IPersona;
import venegas.salazar.bl.dao.persona.SqlServerPersonaDao;
import venegas.salazar.bl.dao.puertas.IPuertaDeAbordaje;
import venegas.salazar.bl.dao.puertas.SqlServerPuertaDeAbordajeDao;
import venegas.salazar.bl.dao.tiquete.ITiquete;
import venegas.salazar.bl.dao.tiquete.SqlServerTiqueteDao;
import venegas.salazar.bl.dao.tripulacion.ITripulacion;
import venegas.salazar.bl.dao.tripulacion.SqlServerTripulacionDao;
import venegas.salazar.bl.dao.usuario.IUsuario;
import venegas.salazar.bl.dao.usuario.SqlServerUsuarioDao;
import venegas.salazar.bl.dao.vuelo.IVuelo;
import venegas.salazar.bl.dao.vuelo.SqlServerVueloDao;


/**
 *
 * @author pc
 */
public class SqlServerDaoFactory extends DaoFactory{
     
    @Override
    public IAdministrador getAdministrador(){
        return new SqlServerAdministradorDao();
    }
    @Override
    public IAeronave getAeronave(){
        return new SqlServerAeronaveDao();
    }
    
     @Override
    public IAeropuerto getAeropuerto(){
        return new SqlServerAeropuertoDao();
    }
    
    
    @Override
    public ILineaAerea getLineaAerea(){
        return new SqlServerLineaAereaDao();
    }
     @Override
    public IMiembro getMiembro(){
        return new SqlServerMiembroDao();
    }
     @Override
    public IPais getPais(){
        return new SqlServerPaisDao();
    }
     @Override
    public IPersona getPersona(){
        return new SqlServerPersonaDao();
    }
     @Override
    public IPuertaDeAbordaje getPuertasDeAbordaje(){
        return new SqlServerPuertaDeAbordajeDao();
    }
    @Override
    public ITiquete getTiquete(){
        return new SqlServerTiqueteDao();
    }
     @Override
    public ITripulacion getTripulacion(){
        return new SqlServerTripulacionDao();
    }
     @Override
    public IUsuario getUsuario(){
        return new SqlServerUsuarioDao();
    }
    
    @Override
    public IVuelo getVuelo(){
        return new SqlServerVueloDao();
    }
    
    
   

}