/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.factory;

import venegas.salazar.bl.dao.admin.IAdministrador;
import venegas.salazar.bl.dao.aeronave.IAeronave;
import venegas.salazar.bl.dao.aeropuerto.IAeropuerto;
import venegas.salazar.bl.dao.lineas.ILineaAerea;
import venegas.salazar.bl.dao.miembro.IMiembro;
import venegas.salazar.bl.dao.pais.IPais;

import venegas.salazar.bl.dao.persona.IPersona;
import venegas.salazar.bl.dao.puertas.IPuertaDeAbordaje;
import venegas.salazar.bl.dao.tiquete.ITiquete;
import venegas.salazar.bl.dao.tripulacion.ITripulacion;
import venegas.salazar.bl.dao.usuario.IUsuario;
import venegas.salazar.bl.dao.vuelo.IVuelo;



/**
 *
 * @author pc
 */
public abstract class DaoFactory {

    public static final int SQLSERVER = 1;

    public static DaoFactory getDaoFactory(int whichFactory) {
        switch (whichFactory) {
            case 1:
                return new SqlServerDaoFactory();

            default:
                return null;
        }
    }

    public abstract IAdministrador getAdministrador();

    public abstract IAeronave getAeronave();

    public abstract IAeropuerto getAeropuerto();

    public abstract ILineaAerea getLineaAerea();

    public abstract IMiembro getMiembro();

    public abstract IPais getPais();

    public abstract IPersona getPersona();

    public abstract IPuertaDeAbordaje getPuertasDeAbordaje();

    public abstract ITiquete getTiquete();

    public abstract ITripulacion getTripulacion();

    public abstract IUsuario getUsuario();

    public abstract IVuelo getVuelo();


    

   

}

