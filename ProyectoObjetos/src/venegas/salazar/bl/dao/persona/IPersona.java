/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.persona;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IPersona {
     void insertar(String nombre, String apellidos, String nacionalidad,
            String genero, String id, String correo, String direccion)throws SQLException,Exception;
      ArrayList<Persona> getPersona() throws SQLException, Exception;
}
