/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.persona;

public class Persona {

    protected String nombre;
    protected String apellidos;
    protected String nacionalidad;
    protected String genero;
    protected String identificacion;
    protected String correo;
    protected String direccion;

    public Persona() {
    }

    public Persona(String nombre, String apellidos, String nacionalidad,
            String genero, String identificacion, String correo, String direccion) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nacionalidad = nacionalidad;
        this.genero = genero;
        this.identificacion = identificacion;
        this.correo = correo;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre
                + ", apellidos=" + apellidos
                + ", nacionalidad=" + nacionalidad
                + ", genero=" + genero
                + ", identificaion=" + identificacion
                + ", correo=" + correo
                + ", direccion=" + direccion + '}';
    }

}
