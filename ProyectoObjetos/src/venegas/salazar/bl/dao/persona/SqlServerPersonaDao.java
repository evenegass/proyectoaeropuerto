/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.persona;

import accesodatos.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerPersonaDao implements IPersona{
    @Override
    public void insertar(String nombre, String apellidos, String nacionalidad,
            String genero, String identificacion, String correo, String direccion)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO PERSONA (NOMBRE,APELLIDOS, NACIONALIDAD, GENERO, ID, CORREO,DIRECCION"
                + "VALUES (" + nombre + ",'" + apellidos+",'"+nacionalidad+",'"+genero+",'"+identificacion+",'"+correo+",'"+direccion+"')";

        Conector.getConector().ejectuarSql(queryText);
    }

    @Override
    public ArrayList<Persona> getPersona() throws Exception {
        ArrayList<Persona> persona = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT NOMBRE, APELLIDOS, NACIONALIDAD, GENERO, ID, CORREO, DIRECCION FROM PERSONA ";
        //CONECTARBD
        rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Persona tmpPersona = new Persona(
                    rs.getString("NOMBRE"),
                       rs.getString("APELLIDOS"),
                       rs.getString("NACIONALIDAD"),
                       rs.getString("GENERO"),
                       rs.getString("ID"),
                       rs.getString("CORREO"),
                    rs.getString("DIRECCION"));
            persona.add(tmpPersona);

        }

        return persona;

    }
}
