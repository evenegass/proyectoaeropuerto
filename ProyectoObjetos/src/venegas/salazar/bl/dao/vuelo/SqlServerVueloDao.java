/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.vuelo;

import accesodatos.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerVueloDao implements IVuelo {

    @Override
    public void insertar(String codigoAeropuerto, String paisOrigen,
            String precioTiquetes, String horaLlegada, String horaSalida,
            String estado, String aeropuertoPaisDestino, String codigoLinea, String codigoPuerta, String codigoVuelo, String codigoTiquete)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO VUELO (CODIGO_AEROPUERTO, PAIS_ORIGEN, PRECIO_TIQUETES, HORA_LLEGADA, HORA_SALIDA, ESTADO, AEROPUERTO_PAIS_DESTINO, CODIGO_LINEA, CODIGO_PUERTA, CODIGO_VUELO, CODIGO_TIQUETE"
                + "VALUES ('" + codigoAeropuerto + "','" + paisOrigen + "','" + precioTiquetes + "','" + horaLlegada + "','" + horaSalida + "','" + estado + "','" + aeropuertoPaisDestino + "','" + codigoLinea + "','" + codigoPuerta + "','" + codigoVuelo + "','"+ codigoTiquete+ "')";

        Conector.getConector().ejectuarSql(queryText);
    }

    @Override
    public ArrayList<Vuelo> getVuelo() throws Exception {
        ArrayList<Vuelo> vuelo = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT CODIGO_AEROPUERTO, PAIS_ORIGEN, PRECIO_TIQUETES, HORA_LLEGADA, HORA_SALIDA, ESTADO, AEROPUERTO_PAIS_DESTINO, CODIGO_LINEA, CODIGO_PUERTA, CODIGO_VUELO, CODIGO_TIQUETE FROM VUELO ";
        //CONECTARBD
        rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Vuelo tmpVuelo = new Vuelo(
                    rs.getString("CODIGO_AEROPUERTO"),
                    rs.getString(" PAIS_ORIGEN"),
                    rs.getString("PRECIO_TIQUETES"),
                    rs.getString("HORA_LLEGADA"),
                    rs.getString("HORA_SALIDA"),
                    rs.getString("ESTADO"),
                    rs.getString("AEROPUERTO_PAIS_DESTINO"),
                    rs.getString("CODIGO_LINEA"),
                    rs.getString("CODIGO_PUERTA"),
                    rs.getString("CODIGO_VUELO"),
                    rs.getString("CODIGO_TIQUETE"));
            vuelo.add(tmpVuelo);

        }

        return vuelo;

    }
}
