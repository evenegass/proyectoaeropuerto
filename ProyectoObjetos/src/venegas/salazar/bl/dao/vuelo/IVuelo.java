/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.vuelo;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IVuelo {
    void insertar( String codigoAeropuerto, String paisOrigen,
            String precioTiquetes, String horaLlegada, String horaSalida,
            String estado, String aeropuertoPaisDestino, String codigoLinea,  String codigoPuerta, String codigoVuelo, String codigoTiquete)throws SQLException,Exception;
      ArrayList<Vuelo> getVuelo() throws SQLException, Exception;
}
