package venegas.salazar.bl.dao.vuelo;

public class Vuelo {

   
    protected String codigoAeropuerto;
    protected String paisOrigen;
    protected String precioTiquetes;
    protected String horaLlegada;
    protected String horaSalida;
    protected String estado;
    protected String aeropuertoPaisDestino;
    protected String codigoLinea;
    protected String codigoPuerta;
    protected String codigoVuelo;
     protected String codigoTiquete;

    public Vuelo() {
    }

    public Vuelo( String codigoAeropuerto, String paisOrigen,
            String precioTiquetes, String horaLlegada, String horaSalida,
            String estado, String aeropuertoPaisDestino, String codigoLinea, String codigoPuerta,String codigoVuelo, String codigoTiquete ) {
     
        this.codigoAeropuerto = codigoAeropuerto;
        this.paisOrigen = paisOrigen;
        this.precioTiquetes = precioTiquetes;
        this.horaLlegada = horaLlegada;
        this.horaSalida = horaSalida;
        this.estado = estado;
        this.aeropuertoPaisDestino = aeropuertoPaisDestino;
        this.codigoLinea= codigoLinea;
        this.codigoPuerta= codigoPuerta;
        this.codigoVuelo= codigoVuelo;
        this.codigoTiquete= codigoTiquete;
    }

   


    public String getCodigoAeropuerto() {
        return codigoAeropuerto;
    }

    public void setCodigoAeropuerto(String codigoAeropuerto) {
        this.codigoAeropuerto = codigoAeropuerto;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public String getPrecioTiquetes() {
        return precioTiquetes;
    }

    public void setPrecioTiquetes(String precioTiquetes) {
        this.precioTiquetes = precioTiquetes;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getAeropuertoPaisDestino() {
        return aeropuertoPaisDestino;
    }

    public void setAeropuertoPaisDestino(String aeropuertoPaisDestino) {
        this.aeropuertoPaisDestino = aeropuertoPaisDestino;
    }

    public String getCodigoLinea() {
        return codigoLinea;
    }

    public void setCodigoLinea(String codigoLinea) {
        this.codigoLinea = codigoLinea;
    }

    public String getCodigoPuerta() {
        return codigoPuerta;
    }

    public void setCodigoPuerta(String codigoPuerta) {
        this.codigoPuerta = codigoPuerta;
    }

    public String getCodigoVuelo() {
        return codigoVuelo;
    }

    public void setCodigoVuelo(String codigoVuelo) {
        this.codigoVuelo = codigoVuelo;
    }

    public String getCodigoTiquete() {
        return codigoTiquete;
    }

    public void setCodigoTiquete(String codigoTiquete) {
        this.codigoTiquete = codigoTiquete;
    }
    

    @Override
    public String toString() {
        return "Vuelo{" + "codigoAeropuerto=" + codigoAeropuerto + ", paisOrigen=" + paisOrigen + ", precioTiquetes=" + precioTiquetes + ", horaLlegada=" + horaLlegada + ", horaSalida=" + horaSalida + ", estado=" + estado + ", aeropuertoPaisDestino=" + aeropuertoPaisDestino + ", codigoLinea=" + codigoLinea + ", codigoPuerta=" + codigoPuerta + ", codigoVuelo=" + codigoVuelo + '}';
    }

   

   
    

    

}
