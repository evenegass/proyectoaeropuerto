/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.usuario;

import accesodatos.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerUsuarioDao implements IUsuario {

    @Override
    public void insertar(String nombre, String apellidos, String nacionalidad, 
            String genero, String identificacion, String correo, String direccion, String codigoUsuario)
            throws SQLException, Exception {

        String queryText;
        queryText = "INSERT INTO USUARIO (CODIGO_USUARIO,NOMBRE,APELLIDOS, NACIONALIDAD, GENERO, IDENTIFICACION, CORREO, DIRECCION"
                + "VALUES ('" + nombre + ",'" + apellidos + ",'" + nacionalidad + ",'" + genero + ",'" + identificacion + ",'" + correo + ",'" + direccion + ",'" + codigoUsuario + "')";

        Conector.getConector().ejectuarSql(queryText);
    }

    @Override
    public ArrayList<Usuario> getUsuario() throws Exception {
        ArrayList<Usuario> usuario = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT  CODIGO_USUARIO,NOMBRE,APELLIDOS, NACIONALIDAD, GENERO, IDENTIFICACION, CORREO, DIRECCION FROM USUARIO ";
        //CONECTARBD
        rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Usuario tmpUsuario = new Usuario(
                    rs.getString("CODIGO_USUARIO"),
                    rs.getString("NOMBRE"),
                    rs.getString("APELLIDOS"),
                    rs.getString("NACIONALIDAD"),
                    rs.getString("GENERO"),
                    rs.getString("IDENTIFICACION"),
                    rs.getString("CORREO"),
                    rs.getString("DIRECCION")
                    );
            usuario.add(tmpUsuario);

        }

        return usuario;

    }
}
