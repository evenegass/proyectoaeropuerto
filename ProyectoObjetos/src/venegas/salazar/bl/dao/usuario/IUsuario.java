/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.usuario;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IUsuario {
      void insertar(String nombre, String apellidos, String nacionalidad, String genero, String identificacion, String correo, String direccion, String codigoUsuario)throws SQLException,Exception;
      ArrayList<Usuario> getUsuario() throws SQLException, Exception;
}
