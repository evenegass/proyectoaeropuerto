package venegas.salazar.bl.dao.usuario;

import venegas.salazar.bl.dao.persona.Persona;

public class Usuario extends Persona {
    
    private String codigoUsuario;

    public Usuario() {
    }

    public Usuario(String nombre, String apellidos, String nacionalidad, String genero, String identificacion, String correo, String direccion,String codigoUsuario) {
        super(nombre, apellidos, nacionalidad, genero, identificacion, correo, direccion);
        this.codigoUsuario=codigoUsuario;
    }

   

    

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setId(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }
    

    @Override
    public String toString() {
        return "Usuario{"  +super.toString()+ '}';
    }

}
