/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.aeropuerto;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IAeropuerto {
    public void insertar(String nombre, String codigoAeropuerto, String pais, String codigoDePais,String codigoAdministrador)throws SQLException,Exception;
      ArrayList<Aeropuerto> getAeropuerto() throws SQLException, Exception; 
}
