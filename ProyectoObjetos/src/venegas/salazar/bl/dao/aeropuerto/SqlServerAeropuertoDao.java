/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venegas.salazar.bl.dao.aeropuerto;

import accesodatos.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public class SqlServerAeropuertoDao implements IAeropuerto {

    

    
     @Override
     public void insertar(String nombre, String codigoAeropuerto, String pais, String codigoDePais, String codigoAdministrador)
           throws SQLException,Exception {

        String queryText;
        queryText = "INSERT INTO AEROPUERTO (NOMBRE,CODIGO_AEROPUERTO,PAIS,CODIGO_PAIS, CODIGO_ADMIN"
                + "VALUES ('" + nombre + "','" + codigoAeropuerto + "','" + pais
                + "','" + codigoDePais+"','"+codigoAdministrador+ "')";

        Conector.getConector().ejectuarSql(queryText);
    }
     @Override
     public ArrayList<Aeropuerto> getAeropuerto() throws Exception {
        ArrayList<Aeropuerto> aeropuerto = new ArrayList<>();
        String queryText;
        ResultSet rs;
        queryText = "SELECT NOMBRE, CODIGO_AEROPUERTO, PAIS, CODIGO_PAIS, CODIGO_ADMIN FROM AEROPUERTO ";
      //CONECTARBD
      rs = Conector.getConector().ejecutarQuery(queryText);

        while (rs.next()) {
            Aeropuerto tmpAerop = new Aeropuerto(
                    rs.getString("NOMBRE"),
                    rs.getString("CODIGO_AEROPUERTO"),
                    rs.getString("PAIS"),
                    rs.getString("CODIGO_PAIS"),
                    rs.getString("CODIGO_ADMIN"));
            aeropuerto.add(tmpAerop);

        }

        return aeropuerto;

    }

}
