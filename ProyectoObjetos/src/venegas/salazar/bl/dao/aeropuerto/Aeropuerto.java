package venegas.salazar.bl.dao.aeropuerto;

public class Aeropuerto {

    private String nombre;
    private String codigoAeropuerto;
    private String pais;
    private String codigoDePais;
    private String codigoAdministrador;

    public Aeropuerto() {
    }

    public Aeropuerto(String nombre, String codigoAeropuerto, String pais, String codigoDePais,String codigoAdministrador) {
        this.nombre = nombre;
        this.codigoAeropuerto = codigoAeropuerto;
        this.pais = pais;
        this.codigoDePais = codigoDePais;
        this.codigoAdministrador=codigoAdministrador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoAeropuerto() {
        return codigoAeropuerto;
    }

    public void setCodigo(String codigoAeropuerto) {
        this.codigoAeropuerto = codigoAeropuerto;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCodigoDePais() {
        return codigoDePais;
    }

    public void setCodigoDePais(String codigoDePais) {
        this.codigoDePais = codigoDePais;
    }

    public String getCodigoAdministrador() {
        return codigoAdministrador;
    }

    public void setCodigoAdministrador(String codigoAdministrador) {
        this.codigoAdministrador = codigoAdministrador;
    }

    @Override
    public String toString() {
        return "Aeropuerto{" + "nombre=" + nombre + ", codigoAeropuerto=" + codigoAeropuerto + ", pais=" + pais + ", codigoDePais=" + codigoDePais + ", codigoAdministrador=" + codigoAdministrador + '}';
    }
    

    

}
