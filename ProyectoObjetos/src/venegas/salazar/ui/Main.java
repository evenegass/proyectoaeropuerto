package venegas.salazar.ui;

import java.io.*;
import java.time.LocalDate;

public class Main {

    public static BufferedReader leer = new BufferedReader(
            new InputStreamReader(System.in));
    public static PrintStream imprimir = System.out;

    public static void main(String[] args) throws IOException {

        crearMenu();

    }

    public static void crearMenu() throws IOException {

        boolean noSalir;
        int opcion;
        do {

            mostrarMenu();
            opcion = leerOpcion();
            noSalir = ejecutarAccion(opcion);

        } while (noSalir);

    }

    public static void mostrarMenu() throws IOException {

        imprimir.println("1.  Registrar personas");
        imprimir.println("2.  Listar persona");
        imprimir.println("3.  Registrar Aeronave");
        imprimir.println("4.  Listar Aeronave");
        imprimir.println("5.  Registrar Aeropuerto");
        imprimir.println("6.  Listar Aeropuerto");
        imprimir.println("7.  Registrar Lineas Aereas");
        imprimir.println("8.  Listar Lineas Aereas");
        imprimir.println("9.  Registrar Paises");
        imprimir.println("10. Listar Paises");
        imprimir.println("11. Registrar Puertas de Abordaje");
        imprimir.println("12. Listar Puertas de Abordaje");
        imprimir.println("13. Registrar Tiquetes");
        imprimir.println("14. Listar Tiquetes");
        imprimir.println("15. Registrar Tripulación");
        imprimir.println("16. Listar Tripulación");
        imprimir.println("17. Registrar Vuelo");
        imprimir.println("18. Listar Vuelo");
        imprimir.println("19. Salir");

        imprimir.println("");
    }// FIN MOSTRAR  MENÚ

    public static int leerOpcion() throws IOException {
        int opcion;

        imprimir.println("Seleccione su opción que desee:\n");
        opcion = Integer.parseInt(leer.readLine());
        imprimir.println();
        return opcion;

    }// FIN LEER OPCION

    public static boolean ejecutarAccion(int popcion) throws IOException {

        boolean noSalir = true;
        boolean define;
        switch (popcion) {

            case 1:

                String tipo = "";

                imprimir.println("Digite que persona quiere registrar");

                imprimir.println("Usuario=1, Administrador= 2, Miembro= 3");
                tipo = leer.readLine();

                if (tipo.equals("1")) {
                    registrarUsuario();
                } else {
                    if (tipo.equals("2")) {
                        registrarAdministrador();
                    } else {
                        if (tipo.equals("3")) {

                            registrarMiembro();
                        }

                    }
                }

                //registrarPersona();
                break;
            case 2:
                listarPersona();

                break;
            case 3:
                registrarAeronave();

                break;

            case 4:
                listarAeronave();

                break;
            case 5:
                RegistrarAeropuerto();

                break;

            case 6:
                ListarAeropuerto();

                break;

            case 7:
                RegistrarLineas();

                break;

            case 8:
                listarLineas();

                break;

            case 9:
                RegistrarPaises();

                break;

            case 10:
                listarPaises();

                break;

            case 11:
                RegistrarPuertasAbordaje();

                break;

            case 12:
                listarPuertasAbordaje();

                break;

            case 13:
                RegistrarTiquetes();

                break;

            case 14:
                listarTiquetes();

                break;

            case 15:
                RegistrarTripulacion();

                break;

            case 16:
                listarTripulacion();

                break;

            case 17:
                RegistrarVuelo();

                break;

            case 18:
                listarVuelo();

                break;

            case 19:
                imprimir.println("Adiós");
                noSalir = false;
                break;

            default:
                imprimir.println("¡OPCIÓN INVÁLIDA!" + "\n"
                        + "Por favor inténtelo nuevamente");
                imprimir.println();
                break;
        }

        return noSalir;

    }

    public static void registrarUsuario() throws IOException {
        String nombre;
        String apellidos;
        String nacionalidad;
        char genero;
        String id;
        String correo;
        String direccion;

        imprimir.println("Digite el nombre del usuario");
        nombre = leer.readLine();
        imprimir.println("Digite los apellidos");
        apellidos = leer.readLine();
        imprimir.println("Digite la nacionalidad");
        nacionalidad = leer.readLine();
        imprimir.println("Digite el genero");
        genero = leer.readLine().charAt(0);
        imprimir.println("Digite la identificación");
        id = leer.readLine();
        imprimir.println("Digite el correo");
        correo = leer.readLine();
        imprimir.println("Digite la dirección");
        direccion = leer.readLine();

    }

    public static void registrarAdministrador() throws IOException {
        String nombre;
        String apellidos;
        String nacionalidad;
        char genero;
        String id;
        String correo;
        String direccion;
        LocalDate fecNacimiento;
        String INputFecha;
        int edad;

        imprimir.println("Digite el nombre del usuario");
        nombre = leer.readLine();
        imprimir.println("Digite los apellidos");
        apellidos = leer.readLine();
        imprimir.println("Digite la nacionalidad");
        nacionalidad = leer.readLine();
        imprimir.println("Digite el genero");
        genero = leer.readLine().charAt(0);
        imprimir.println("Digite la identificación");
        id = leer.readLine();
        imprimir.println("Digite el correo");
        correo = leer.readLine();
        imprimir.println("Digite la dirección");
        direccion = leer.readLine();
        imprimir.println("Digite la fecha de nacimiento");
        INputFecha = leer.readLine();
        fecNacimiento = LocalDate.parse(INputFecha);
        imprimir.println("Digite la edad");
        edad = Integer.parseInt(leer.readLine());

    }

    public static void registrarMiembro() throws IOException {
        String nombre;
        String apellidos;
        String nacionalidad;
        char genero;
        String id;
        String correo;
        String direccion;
        int anniosExp;
        LocalDate fecGraduacion;
        String licencia;
        String puesto;
        String telefono;
        
        imprimir.println("Digite el nombre del usuario");
        nombre = leer.readLine();
        imprimir.println("Digite los apellidos");
        apellidos = leer.readLine();
        imprimir.println("Digite la nacionalidad");
        nacionalidad = leer.readLine();
        imprimir.println("Digite el genero");
        genero = leer.readLine().charAt(0);
        imprimir.println("Digite la identificación");
        id = leer.readLine();
        imprimir.println("Digite el correo");
        correo = leer.readLine();
        imprimir.println("Digite la dirección");
        direccion = leer.readLine();
        

    }

    public static void listarPersona() {

    }

    public static void registrarAeronave() {

    }

    public static void listarAeronave() {

    }

    public static void RegistrarAeropuerto() {

    }

    public static void ListarAeropuerto() {

    }

    public static void RegistrarLineas() {

    }

    public static void listarLineas() {

    }

    public static void RegistrarPaises() {

    }

    public static void listarPaises() {

    }

    public static void RegistrarPuertasAbordaje() {

    }

    public static void listarPuertasAbordaje() {

    }

    public static void RegistrarTiquetes() {

    }

    public static void listarTiquetes() {

    }

    public static void RegistrarTripulacion() {

    }

    public static void listarTripulacion() {

    }

    public static void RegistrarVuelo() {

    }

    public static void listarVuelo() {

    }

}
